#include "LedExt.h"
#include "MsgService.h"
#include "states.h"
#include "servo_motor_impl.h"
#include "Arduino.h"
#include "SoftwareSerial.h"
#include <string.h>

#define TX_PIN 3
#define RX_PIN 2
#define STD_BRIGHTNESS 0
#define MIN_BRIGHTNESS 85
#define MED_BRIGHTNESS 170
#define MAX_BRIGHTNESS 255
#define LED_PIN 7
#define LED_PIN2 8
#define LED_PIN3 6
#define SERVO_PIN 5
#define TRIG_PIN 11
#define ECHO_PIN 12
#define MED_POSITION 90
#define MAX_POSITION 180
#define MIN_POSITION 45
#define STOP_POSITION 0
#define vs 331.45 + 0.62 * 20

LightExt* led1;
LightExt* led2;
LightExt* led3;
char data;
ServoMotor* pMotor;
status working_status = AUTO;
long duration;
int distance;
bool manual_mode = false;

SoftwareSerial channel(RX_PIN, TX_PIN);

void setup()
{
    pinMode(TRIG_PIN, OUTPUT);
    pinMode(ECHO_PIN, INPUT);

    enum status {
        AUTO,
        MANUAL
    };

    pMotor = new ServoMotorImpl(SERVO_PIN);
    led1 = new LedExt(LED_PIN, STD_BRIGHTNESS);
    led2 = new LedExt(LED_PIN2, STD_BRIGHTNESS);
    led3 = new LedExt(LED_PIN3, STD_BRIGHTNESS);
    led1->switchOn();
    led2->switchOn();
    led3->switchOn();
    MsgService.init();
    channel.begin(9600);
    pMotor->on();
    pMotor->setPosition(MAX_POSITION);
    delay(1000);
    pMotor->off();
}

int getDistance()
{
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(5);
    // Trigger the sensor by setting the trigPin high for 10 microseconds:
    digitalWrite(TRIG_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(TRIG_PIN, LOW);
    // Read the echoPin, pulseIn() returns the duration (length of the pulse) in microseconds:
    duration = pulseIn(ECHO_PIN, HIGH);
    // Calculate the distance:
    distance = duration * 0.034 / 2;

    delay(50);
    return distance;
}

void loop()
{ 
            int distanceCheck = getDistance();
            if (distanceCheck <= 4 && manual_mode == false) 
            {
                led1->setIntensity(STD_BRIGHTNESS);
                led2->setIntensity(STD_BRIGHTNESS);
                led3->setIntensity(MAX_BRIGHTNESS);
                MsgService.sendMsg("MANUAL");
                manual_mode = true;
                working_status = MANUAL;
                
            }
            else if (!manual_mode)
            {
                led1->setIntensity(MAX_BRIGHTNESS);
                led2->setIntensity(STD_BRIGHTNESS);
                led3->setIntensity(STD_BRIGHTNESS);
                MsgService.sendMsg("AUTO");
                delay(250);
                working_status = AUTO;
                
            }
  
    
    switch (working_status) 
    {
    case AUTO:
      
            if (MsgService.isMsgAvailable()) 
            {
                Msg* msg = MsgService.receiveMsg();

                if (msg->getContent() == "MAX") 
                {
                    led2->setIntensity(MAX_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(MAX_POSITION);
                    delay(1000);
                    pMotor->off();
                    delay(3000);
                }
                else if (msg->getContent() == "MED") 
                {
                    led2->setIntensity(MED_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(MED_POSITION);
                    delay(1000);
                    pMotor->off();
                    delay(3000);
                }
               else if (msg->getContent() == "MIN") 
               {
                    led2->setIntensity(MIN_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(MIN_POSITION);
                    delay(1000);
                    pMotor->off();
                    delay(3000);
                }
              else  if (msg->getContent() == "STOP") 
              {
                    pMotor->on();
                    pMotor->setPosition(STOP_POSITION);
                    delay(1000);
                    pMotor->off();
                    delay(3000);
                }
                delete msg;
            }
        

        break;
    case MANUAL:
      

        while(working_status == MANUAL){
            delay(500);
            MsgService.sendMsg("MANUAL");

             if (channel.available()) 
            {
                String msgBT = "";
                while (channel.available()) 
                {
                    msgBT += (char)channel.read();
                }

                if (msgBT == "1.") 
                { //stop button

                MsgService.sendMsg("STOP");
                    led2->setIntensity(STD_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(STOP_POSITION);
                    delay(500);
                    pMotor->off();
                    
                    delay(500);
                }
                if (msgBT == "2.") 
                { // min Button
                    MsgService.sendMsg("MIN");
                    led2->setIntensity(MIN_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(MIN_POSITION);
                    delay(500);
                    pMotor->off();
                    
                    delay(500);
                }
                if (msgBT == "3.")          
                { //med button
                    MsgService.sendMsg("MED");
                    led2->setIntensity(MED_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(MED_POSITION);
                    delay(500);
                    pMotor->off();
                    
                    delay(500);
                }
                if (msgBT == "4.") 
                { // max Button
                    MsgService.sendMsg("MAX");
                    led2->setIntensity(MAX_BRIGHTNESS);
                    pMotor->on();
                    pMotor->setPosition(MAX_POSITION);
                    delay(500);
                    pMotor->off();
                    
                    delay(500);
                }
                if (msgBT == "5.") 
                { //exit button
                    manual_mode = false;
                    working_status = AUTO;
                }
            }

        }
        break;

    default:
        break;
    } // switch close
}
